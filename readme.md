## p2phx ##

Haxe peer-to-peer library.

### Description ###

None yet.

### Installation ###

`haxelib git p2phx http://gitlab.sheridan.edu.au/sri/p2phx.git`

### Usage ###

None yet.

### Known bugs ###

- Won't work under Windows NekoVM due to ["bug"](https://github.com/HaxeFoundation/neko/issues/93).
- UDP proto only.

### Contacts ###

Arthur "nibb13" Makhonko: <mailto:nibble@list.ru>
Michael "maoshin" Adeyeye Oshin: <mailto:madeyeye@sheridan.edu.au>
