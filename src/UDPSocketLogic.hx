package;
import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import neko.vm.Thread;
import sys.net.Address;
import sys.net.Host;
import sys.net.UdpSocket;

import SocketLogic.SockState;

/**
 * ...
 * @authors Arthur Makhonko, Michael Adeyeye Oshin
 */

/*enum SockState {
	
	NONE;
	BOUND;
	LISTENING;
	CONNECTED;
	CLOSED;
	
}*/

enum MessageType {
	
	CHECK;
	INIT;
	INIT_RESPONSE;
	PAYLOAD;
	RESEND_CHUNKS;
	FINISH;
	FINISH_RESPONSE;
	
}
 
class UDPSocketLogic extends SocketLogic {
	
	private static inline var CHUNK_PAYLOAD_LENGTH:Int = 1012;
	
	public var done(get, null):Bool;
	public var localHost(get, null):String;
	public var localPort(get, null):Int;
	public var remoteHost(get, null):String;
	public var remotePort(get, null):Int;
	public var sockState(get, null):SockState;
	public var remoteSockState(get, null):SockState;

	private var socket:UdpSocket;
	private var host:String;
	private var port:UInt;
	private var _sockState:SockState;
	private var _remoteSockState:SockState;
	
	private var messages:Array<String> = new Array<String>();
	private var sendingMessage:String;
	private var sendingMessageNum:Int = 0;
	private var sendingMessageNumResponse:Int = -1;
	private var sendingChunks:Map<Int, Bytes>;
	private var chunkIsSent:Map<Int, Bool>;
	
	private var receivingChunks:Map<Int, Bytes>;
	private var receivingMessageLength:Int = 0;
	
	private var address:Address;
	private var assembledMessage:String;

	public function new(host:String, port:UInt) {
		
		super();
		
		this._sockState = this._remoteSockState = SockState.NONE;
		this.host = host;
		this.port = port;
		
		if (host != null && port != 0) {
			this.address = new Address();
			this.address.host = new Host(host).ip;
			this.address.port = port;
		}
		
	}
	
	function get_done():Bool {
		
		return this._sockState == SockState.CLOSED && this.socket != null;
		
	}
	
	function get_localHost():String {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.socket.host().host.toString();
		}
		
		return null;
		
	}
	function get_localPort():Int {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.socket.host().port;
		}
		
		return 0;
		
	}
	
	function get_remoteHost():String {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.host;
		}
		
		return null;
		
	}
	
	function get_remotePort():Int {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.port;
		}
		
		return 0;
		
	}
	
	function get_sockState():SockState {
		
		return _sockState;
		
	}
	
	function get_remoteSockState():SockState {
		return _remoteSockState;
	}
	
	public function bind(host:String, port:Int) {
		
		this.socket = _bind({ host:host, port:port });
		
	}
	
	public function listen() {
		
		if (this._sockState == SockState.BOUND) {
			Thread.create(function() {
				//this.socket.listen(1);
				this.socket.setBlocking(false);
				this._sockState = SockState.LISTENING;
				this.run();
			});
		}
		
	}
	
	public function connect(?host:String = null, ?port:Null<Int> = null, ?bindTo: { host:String, port:Int } = null) {
		
		Thread.create(function() {
			if (this._sockState == SockState.NONE || this._sockState == SockState.BOUND) {
				this.socket = _connect(host, port, bindTo);
				if (this.socket != null) {
					if (this.onConnect != null) this.onConnect();
					this.run();
				}
			}
		});
		
	}
	
	public function closeConnection() {

		if (this.socket != null) this.socket.close();
		this._sockState = SockState.CLOSED;
		if (this.onConnectionClose != null) this.onConnectionClose();
		  
	}
	
	override public function send(message:Dynamic) {
		
		if (this._sockState != SockState.CONNECTED && this._sockState != SockState.LISTENING) {
			throw "Can't send, socket is closed.";
			return;
		}
		
		if (Std.is(message, String)) {
			var messageStr:String = cast(message, String);
			messages.push(messageStr);
		} else {
			throw "Only strings are allowed to send at the moment.";
		}
		
	}
	
	public dynamic function onConnect() {
		
	}
	
	public dynamic function onData(data:Dynamic) {
		
	}
	
	public dynamic function onConnectionClose() {
		
	}
	
	private function _bind(bindTo:{host:String, port:Int}):UdpSocket {
		
		var sock:UdpSocket;
		
		if (this.socket != null && this._sockState == SockState.NONE) {
			sock = this.socket;
		} else {
			sock = new UdpSocket();
		}
		
		var success:Bool = false;
		
		do {
			if (this._sockState == SockState.CLOSED) return null; // handle cancelling connection
			try {
				sock.bind(new Host(bindTo.host), bindTo.port);
				success = true;
			} catch (e:Dynamic) {
				success = false;
			}
		} while (!success);
		
		this._sockState = SockState.BOUND;
		
		Util.log('UDP: Bound to $bindTo');
		
		return sock;
		
	}
	
	private function _connect(?host:String = null, ?port:Null<Int> = null, ?bindTo:{host:String, port:Int} = null):UdpSocket {
		
		var sock:UdpSocket;
		
		if (this._sockState == SockState.BOUND) {
			sock = this.socket;
		} else {
			sock = new UdpSocket();
		}
		
		if (host != null) this.host = host;
		if (port != null) this.port = port;
		
		this.address = new Address();
		this.address.host = new Host(this.host).ip;
		this.address.port = this.port;
	  
		if (bindTo != null) {

			_bind(bindTo);
			
		}
	  
		sock.setBlocking(false);
		
		this._sockState = SockState.CONNECTED;
	  
		return sock;
		
	}
	
	private function _send(msg:Bytes) {
		
		try {
			this.socket.sendTo(msg, 0, msg.length, address);
		} catch (e:Dynamic) { }
		
	}
	
	private function run() {
		
		do {
			
			if (_remoteSockState != SockState.CONNECTED) {
				
				var message:Bytes = Bytes.alloc(12);
				message.setInt32(4, Type.enumIndex(MessageType.CHECK));
				
				_send(message);
				
			}
			
			if (_sockState == SockState.LISTENING) {
				
				// Receive messages
				
				var bufferEmpty:Bool = false;
				
				do {
				
					try {
						
						var buf:Bytes = Bytes.alloc(1024);
						var fromAddress:Address = new Address();
						
						this.socket.readFrom(buf, 0, 1024, fromAddress);
						
						var id:Int = buf.getInt32(0);
						var type:MessageType = Type.createEnumIndex(MessageType, buf.getInt32(4));
						var length:Int = buf.getInt32(8);
						
						switch (type) {
							case MessageType.CHECK:
								
								if (_remoteSockState != SockState.CONNECTED) {
									
									_remoteSockState = SockState.CONNECTED;
									
									if (this.onConnect != null) this.onConnect();
									
								}
								
							case MessageType.INIT:
								
								receivingMessageLength = buf.getInt32(0);
								
								receivingChunks = new Map<Int, Bytes>();
								assembledMessage = null;
								
								var message:Bytes = Bytes.alloc(12);
								
								message.setInt32(0, id);
								message.setInt32(4, Type.enumIndex(MessageType.INIT_RESPONSE));
								_send(message);
								
							case MessageType.INIT_RESPONSE:
								
								sendingMessageNumResponse = id;
								
							case MessageType.PAYLOAD:
								
								//buf = Bytes.alloc(length);
								//this.socket.readFrom(buf, 0, length, fromAddress);
								receivingChunks[id] = buf.sub(12, length);
								
							case MessageType.RESEND_CHUNKS:
								
								//buf = Bytes.alloc(length);
								//this.socket.readFrom(buf, 0, length, fromAddress);
								
								for (i in 0...Std.int(length / 4)) {
									var chunkIndex = buf.getInt32(i * 4 + 12);
									chunkIsSent[chunkIndex] = false;
								}
								
							case MessageType.FINISH:
								
								var lostChunks:Array<Int> = new Array<Int>();
								
								for (i in 0...length) {
									
									if (!receivingChunks.exists(i)) lostChunks.push(i);
									
								}
								
								if (lostChunks.length > 0) {
									
									//var message:Bytes = Bytes.alloc(lostChunks.length * 4 + 12);
									var message:Bytes = Bytes.alloc(1024);
									message.setInt32(0, id);
									message.setInt32(4, Type.enumIndex(MessageType.RESEND_CHUNKS));
									message.setInt32(8, Std.int(Math.min(CHUNK_PAYLOAD_LENGTH, lostChunks.length * 4)));
									
									for (i in 0...Std.int(Math.min(lostChunks.length, Math.floor(CHUNK_PAYLOAD_LENGTH / 4)))) {
										message.setInt32(i * 4 + 12, lostChunks[i]);
									}
									
									_send(message);
									
								} else {
									
									if (assembledMessage == null) {
										assembledMessage = '';
										
										for (i in 0...length) {
											assembledMessage += receivingChunks[i].toString();
										}
										
										if (this.onData != null) this.onData(assembledMessage);
										
									}
									
									var message:Bytes = Bytes.alloc(12);
									message.setInt32(0, id);
									message.setInt32(4, Type.enumIndex(MessageType.FINISH_RESPONSE));
									
									_send(message);
									
								}
								
							case MessageType.FINISH_RESPONSE:
								
								if (id == sendingMessageNum) sendingMessage = null;
								
						}
						
					} catch (e:Dynamic) { bufferEmpty = true; }
				} while (!bufferEmpty);
				
				// Send queued messages
				
				if (sendingMessage == null && messages.length > 0) {
					
					sendingMessage = messages.shift();
					
					if (sendingMessage != null) {
					
						sendingChunks = new Map<Int, Bytes>();
						chunkIsSent = new Map<Int, Bool>();
					
						var i:Int = 0;
						var chunkNum:Int = 0;
						
						do {
							
							var payload = sendingMessage.substr(i, CHUNK_PAYLOAD_LENGTH);
							var chunk:Bytes = Bytes.alloc(payload.length + 12);
							chunk.setInt32(0, chunkNum);
							chunk.setInt32(4, Type.enumIndex(MessageType.PAYLOAD));
							chunk.setInt32(8, payload.length);
							chunk.blit(12, Bytes.ofString(payload), 0, payload.length);
							sendingChunks[chunkNum] = chunk;
							
							chunkNum++;
							i += CHUNK_PAYLOAD_LENGTH;
							
						} while (i < sendingMessage.length);
						
						sendingMessageNum++;
					
					}
					
				}
				
				if (sendingMessage != null) {
					
					if (sendingMessageNum != sendingMessageNumResponse) {
						
						var message = Bytes.alloc(12);
						message.setInt32(0, sendingMessageNum);
						message.setInt32(4, Type.enumIndex(MessageType.INIT));
						
						_send(message);
						//this.socket.sendTo(message, 0, message.length, address);
						
					} else {
						
						var sendCapCounter:Int = 0;
						
						for (chunkNum in sendingChunks.keys()) {
							
							var done:Bool = true;
							
							if (!chunkIsSent[chunkNum]) {
								
								//this.socket.sendTo(chunk, 0, 1024, address);
								_send(sendingChunks[chunkNum]);
								chunkIsSent[chunkNum] = true;
								done = false;
								
								Sys.sleep(0.05);
								
								/*if (++sendCapCounter >= 8388) { // ~8 mb/s TODO: make variable
									break; // prevent UDP flooding
								}*/
								
							}
							
							if (done) {
								
								var message:Bytes = Bytes.alloc(12);
								message.setInt32(0, sendingMessageNum);
								message.setInt32(4, Type.enumIndex(MessageType.FINISH));
								message.setInt32(8, Math.ceil(sendingMessage.length / CHUNK_PAYLOAD_LENGTH));
								_send(message);
								
							}
							
						}
						
					}
					
				}
					
			}
			
			Sys.sleep(0.1); // TODO: make const
		} while ( _sockState == SockState.CONNECTED || _sockState == SockState.LISTENING);
		
	}
	
}
