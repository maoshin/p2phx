package;

/**
 * ...
 * @authors Arthur Makhonko, Michael Adeyeye Oshin
 */

enum SockState {
	NONE;
	BOUND;
	LISTENING;
	CONNECTED;
	CLOSED;
}
 
class SocketLogic {
	
	public var binaryMode:Bool = false;
	public var messageSize:Int;

	public function new() {
		
	}
	
	public function send(message:Dynamic) { }
	
}
