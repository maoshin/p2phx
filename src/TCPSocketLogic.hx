package;
import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import neko.vm.Thread;
import sys.net.Host;
import sys.net.Socket;

import SocketLogic.SockState;

/**
 * ...
 * @authors Arthur Makhonko, Michael Adeyeye Oshin
 */

typedef Line = {
	var line:String;
	var eof:Bool;
}
 
class TCPSocketLogic extends SocketLogic {
	
	public var done(get, null):Bool;
	public var localHost(get, null):String;
	public var localPort(get, null):Int;
	public var remoteHost(get, null):String;
	public var remotePort(get, null):Int;
	public var sockState(get, null):SockState;
	
	private var socket:Socket;
	private var clientSocket:Socket;
	private var host:String;
	private var port:UInt;
	private var _sockState:SockState;

	public function new(host:String, port:UInt) {
		
		super();
		
		this._sockState = SockState.NONE;
		this.host = host;
		this.port = port;
		
	}
	
	function get_done():Bool {
		
		return this._sockState == SockState.CLOSED && this.socket != null;
		
	}
	
	function get_localHost():String {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.socket.host().host.toString();
		}
		
		return null;
		
	}
	function get_localPort():Int {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.socket.host().port;
		}
		
		return 0;
		
	}
	
	function get_remoteHost():String {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.socket.peer().host.toString();
		}
		
		return null;
		
	}
	
	function get_remotePort():Int {
		
		if (this._sockState == SockState.BOUND || this._sockState == SockState.CONNECTED) {
			return this.socket.peer().port;
		}
		
		return 0;
		
	}
	
	function get_sockState():SockState {
		
		return _sockState;
		
	}
	
	public function bind(host:String, port:Int) {
		
		this.socket = _bind({ host:host, port:port });
		
	}
	
	public function listen() {
		
		if (this._sockState == SockState.BOUND) {
			Thread.create(function() {
				this.socket.listen(1);
				this.socket.setBlocking(false);
				this._sockState = SockState.LISTENING;
				this.run();
			});
		}
		
	}
	
	public function connect(?host:String = null, ?port:Null<Int> = null, ?bindTo: { host:String, port:Int } = null) {
		
		Thread.create(function() {
			if (this._sockState == SockState.NONE || this._sockState == SockState.BOUND) {
				this.socket = _connect(host, port, bindTo);
				if (this.socket != null) {
					if (this.onConnect != null) this.onConnect();
					this.run();
				}
			}
		});
		
	}
	
	public function closeConnection() {

		if (this.socket != null) this.socket.close();
		this._sockState = SockState.CLOSED;
		if (this.onConnectionClose != null) this.onConnectionClose();
		  
	}
	
	override public function send(message:Dynamic) {
		
		if (this._sockState != SockState.CONNECTED && this._sockState != SockState.LISTENING) {
			throw "Can't send, socket is closed.";
			return;
		}
		
		if (Std.is(message, String)) {
			this.socket.write(message);
		} else {
			throw "Only strings are allowed to send at the moment.";
		}
		
	}
	
	public dynamic function onConnect() {
		
	}
	
	public dynamic function onData(data:Dynamic) {
		
	}
	
	public dynamic function onConnectionClose() {
		
	}
	
	private function _bind(bindTo:{host:String, port:Int}):Socket {
		
		var sock:Socket;
		
		if (this.socket != null && this._sockState == SockState.NONE) {
			sock = this.socket;
		} else {
			sock = new Socket();
		}
		
		var success:Bool = false;
		
		do {
			if (this._sockState == SockState.CLOSED) return null; // handle cancelling connection
			try {
				sock.bind(new Host(bindTo.host), bindTo.port);
				success = true;
			} catch (e:Dynamic) {
				success = false;
				Util.error(Std.string(e));
			}
		} while (!success);
		
		this._sockState = SockState.BOUND;
		
		Util.log('Bound to $bindTo');
		
		return sock;
		
	}
	
	private function _connect(?host:String = null, ?port:Null<Int> = null, ?bindTo:{host:String, port:Int} = null):Socket {
		
		var sock:Socket;
		
		if (this._sockState == SockState.BOUND) {
			sock = this.socket;
		} else {
			sock = new Socket();
		}
		
		if (host == null) host = this.host;
		if (port == null) port = this.port;
	  
		if (bindTo != null) {

			_bind(bindTo);
			
		}
	  
		var success:Bool = false;
		
		do {
			if (this._sockState == SockState.CLOSED) return null; // handle cancelling connection
			try {
				sock.connect(new Host(host), port);
				success = true;
			} catch (e:Dynamic) {
				if (Std.is(e, Int)) {
					Util.error('Int error: $e');
					Sys.sleep(Math.random());
				} else if (cast(e, String).indexOf('Failed to connect on ') == 0) {
					success = false;
					Util.warn('Error, retrying to $host:$port');
					Sys.sleep(0.1);
				} else if (cast(e, String) == 'std@socket_bind') {
					return null;
				} else {
					Util.error('Error: $e');
					Sys.sleep(1);
				}
			}
		} while (!success);
	  
		sock.setBlocking(false);
		
		this._sockState = SockState.CONNECTED;
	  
		return sock;
		
	}
	
	private function run() {
		
		do {
			
			if (_sockState == SockState.LISTENING) {
				
				var client:Socket = null;
				
				try {
					client = this.socket.accept();
				} catch (e:Dynamic) { }
				
				if (client != null) {
					this.socket.close();
					this.socket = client;
					_sockState = SockState.CONNECTED;
					if (this.onConnect != null) this.onConnect();
				}
				
			} else if (_sockState == SockState.CONNECTED){
		  
				var lineObj:Line = readLine(this.socket);

				if (lineObj.eof) closeConnection();

				if (lineObj.line != '') {
					if (this.onData != null) this.onData(lineObj.line);
				}
				
			}

			Sys.sleep(0.1); // TODO: make const
		} while ( _sockState == SockState.CONNECTED || _sockState == SockState.LISTENING);
		
	}
	
	private function readLine(sock:Socket):Line {
	  
		var l:String = '';
		var done:Bool = false;
		var sockClose:Bool = false;
		
		do {
			try {
				
				if (this.binaryMode) {
					sock.setBlocking(true);
					l = sock.input.readString(this.messageSize);
					sock.setBlocking(false);
					done = true;
				} else {
					var char:Int = sock.input.readByte();
					if (char != 10){
						l += String.fromCharCode(char);
					} else {
						done = true;
					}
				}
			} catch (e:Dynamic) {
				if (Std.string(e) == 'Eof') {
					sockClose = true;
				}
				done = true;
			}
		} while (!done);
		
		return { line:l, eof:sockClose };
	  
	}
	
}
