package;
import haxe.Json;
import sys.net.Host;
import sys.net.Socket;
import SocketLogic.SockState;

/**
 * ...
 * @authors Arthur Makhonko, Michael Adeyeye Oshin
 */

typedef HostPortTuple = {
	var localHost:String;
	var localPort:Int;
	var remoteHost:String;
	var remotePort:Int;
}
 
class P2PLogic {
	
	private var handshakeConn:TCPSocketLogic;
	private var localPeerConn:TCPSocketLogic;
	private var remotePeerConn:TCPSocketLogic;
	private var localUdpPeerConn:UDPSocketLogic;
	private var remoteUdpPeerConn:UDPSocketLogic;
	private var id:String;
	private var host:String;
	private var port:Int;
	private var ourHost:String;
	private var ourPort:Int = 0;
	private var portDelta:Int = 0;
	
	public var done(get, null):Bool;
	
	function get_done():Bool {
		return handshakeConn != null && handshakeConn.done && remotePeerConn != null && remotePeerConn.done;
	}

	public function new(host:String, port:Int, peerid:String) {
		
		this.host = host;
		this.port = port;
		
		id = peerid;
		
		var testConn:Socket = new Socket();
		testConn.connect(new Host(this.host), this.port);
		ourHost = testConn.host().host.toString();
		testConn.close();
		
	}
	
	public function start() {
		
		handshakeConn = new TCPSocketLogic(this.host, this.port);
		handshakeConn.onConnect = this._onHandshakeConnect;
		handshakeConn.onData = this._onHandshakeData;
		handshakeConn.onConnectionClose = this._onHandshakeConnectionClose;
		//handshakeConn.bind(new Host(ourHost).toString(), 0);
		//ourPort = handshakeConn.localPort;
		handshakeConn.bind(new Host(ourHost).toString(), 0);
		//ourPort = 31337;
		handshakeConn.connect();
		
	}
	
	public function pairWith(toPeerId:String) {
		
		if (handshakeConn == null || handshakeConn.sockState != SockState.CONNECTED) {
			throw "No active p2p server connection. Forgot to start()?";
		}
		
		handshakeConn.send('{"cmd":"pairRequest", "toPeerId":"$toPeerId"}\n');
		
	}
	
	public function peerSend(message:Dynamic) {
		
		var activePeer:SocketLogic = null;
		
		if (localPeerConn != null && localPeerConn.sockState == SockState.CONNECTED) activePeer = localPeerConn;
		if (remotePeerConn != null && remotePeerConn.sockState == SockState.CONNECTED) activePeer = remotePeerConn;
		if (localUdpPeerConn != null && localUdpPeerConn.sockState == SockState.LISTENING) activePeer = localUdpPeerConn;
		if (remoteUdpPeerConn != null && remoteUdpPeerConn.sockState == SockState.LISTENING) activePeer = remoteUdpPeerConn;
		
		if (activePeer == null) {
			throw "No active peer found, cannot send!";
		}
		
		activePeer.send(message);
		
	}
	
	public function setBinaryMode(to:Bool) {
		
		var activePeer:SocketLogic = null;
		
		if (localPeerConn != null && localPeerConn.sockState == SockState.CONNECTED) activePeer = localPeerConn;
		if (remotePeerConn != null && remotePeerConn.sockState == SockState.CONNECTED) activePeer = remotePeerConn;
		if (localUdpPeerConn != null && localUdpPeerConn.sockState == SockState.LISTENING) activePeer = localUdpPeerConn;
		if (remoteUdpPeerConn != null && remoteUdpPeerConn.sockState == SockState.LISTENING) activePeer = remoteUdpPeerConn;
		
		if (activePeer == null) {
			throw "No active peer found, cannot set binary mode!";
		}
		
		activePeer.binaryMode = to;
		
	}
	
	public function setMessageSize(to:Int) {
		
		var activePeer:SocketLogic = null;
		
		if (localPeerConn != null && localPeerConn.sockState == SockState.CONNECTED) activePeer = localPeerConn;
		if (remotePeerConn != null && remotePeerConn.sockState == SockState.CONNECTED) activePeer = remotePeerConn;
		if (localUdpPeerConn != null && localUdpPeerConn.sockState == SockState.LISTENING) activePeer = localUdpPeerConn;
		if (remoteUdpPeerConn != null && remoteUdpPeerConn.sockState == SockState.LISTENING) activePeer = remoteUdpPeerConn;
		
		if (activePeer == null) {
			throw "No active peer found, cannot set binary mode!";
		}
		
		activePeer.messageSize = to;
		
	}
	
	public dynamic function onHandshakeConnect() {
		
	}
	
	public dynamic function onHandshakeData(data:Dynamic) {
		
	}
	
	public dynamic function onHandshakeConnectionClose() {
		
	}
	
	public dynamic function onLocalPeerConnect() {
		
	}
	
	public dynamic function onRemotePeerConnect() {
		
	}
	
	public dynamic function onPeerData(data:Dynamic) {
		
	}
	
	public dynamic function onPeerClose() {
		
	}
	
	private function _onHandshakeConnect() {
		
		Util.log('P2P: Connected to p2p server.');
		handshakeConn.send('{"cmd":"setPeerId", "peerId":"${this.id}"}\n');
		if (ourPort != 0){
			handshakeConn.send('{"cmd":"setLocalTuple", "host":"${handshakeConn.localHost}", "port":$ourPort}\n');
		} else {
			handshakeConn.send('{"cmd":"setLocalTuple", "host":"${handshakeConn.localHost}", "port":${handshakeConn.localPort}}\n');
		}
		
		if (onHandshakeConnect != null) onHandshakeConnect();
		
	}
	
	private function _onHandshakeData(data:Dynamic) {
		
		handleHandshakeMessage(cast(data, String));
		
		if (onHandshakeData != null) onHandshakeData(data);
		
	}
	
	private function _onHandshakeConnectionClose() {
		
		if (onHandshakeConnectionClose != null) onHandshakeConnectionClose();
		
	}
	
	private function handleHandshakeMessage(msg:String) {
		
		var msgObj:Dynamic = null;
		
		try {
			 msgObj = Json.parse(msg);
		} catch (e:Dynamic) {}
		
		if (msgObj != null) {
			
			switch (msgObj.cmd) {
				case 'connectTo':
					connectToPeer(msgObj.hostPortTuple, msgObj.delta);
				case 'deltacheckports':
					Util.log('Delta request with ports: ${msgObj.ports}');
					handshakeConn.send('{"cmd":"setDelta", "delta":${findDelta(msgObj.ports)}}\n');
				case 'sendTuple':
					handshakeConn.closeConnection();
					start();
					//handshakeConn.send('{"cmd":"setLocalTuple", "host":"$ourHost", "port":$ourPort}\n');
				default:
					
			}
			
		}
		
	}
	
	private function connectToPeer(hostPortTuple:HostPortTuple, delta:Int) {
		
		Util.log('P2P: Connecting to $hostPortTuple');
		
		handshakeConn.closeConnection();
		
		localUdpPeerConn = new UDPSocketLogic(hostPortTuple.localHost, hostPortTuple.localPort);
		localUdpPeerConn.onConnect = this._onLocalPeerConnect;
		localUdpPeerConn.onData = this._onPeerData;
		localUdpPeerConn.bind('0.0.0.0', ourPort);
		localUdpPeerConn.listen();
		
		Sys.sleep(5);
		
		if (localUdpPeerConn.remoteSockState != SockState.CONNECTED) {
			localUdpPeerConn.closeConnection();
			localUdpPeerConn = null;
			Util.log('P2P: Trying remote connection to ${hostPortTuple.remoteHost}:${hostPortTuple.remotePort}.');
			remoteUdpPeerConn = new UDPSocketLogic(hostPortTuple.remoteHost, hostPortTuple.remotePort);
			remoteUdpPeerConn.onConnect = this._onRemotePeerConnect;
			remoteUdpPeerConn.onData = this._onPeerData;
			remoteUdpPeerConn.bind('0.0.0.0', ourPort);
			remoteUdpPeerConn.listen();
		}
		
	/*	localPeerConn = new SocketLogic(null, null);
		localPeerConn.onConnect = this._onLocalPeerConnect;
		localPeerConn.onData = this._onPeerData;
		localPeerConn.onConnectionClose = this._onPeerClose;
		//localPeerConn.bind(ourHost, ourPort + portDelta);
		//localPeerConn.connect(hostPortTuple.localHost, hostPortTuple.localPort, { host:ourHost, port:ourPort } );
		//localPeerConn.connect(hostPortTuple.localHost, hostPortTuple.localPort, {host:'0.0.0.0', port:ourPort});
		
//		var listenPeerConn = new SocketLogic(null, null);
//		listenPeerConn.onData = function(data:Dynamic) {
//			Util.log('Data from: ${listenPeerConn.remoteHost}:${listenPeerConn.remotePort}' );
//			this._onPeerData(data);
//		}
//		//listenPeerConn.bind(ourHost, ourPort);
//		listenPeerConn.bind('0.0.0.0', ourPort);
//		listenPeerConn.listen();
		
		if (localPeerConn.sockState != SocketLogic.SockState.CONNECTED &&
			(hostPortTuple.localHost != hostPortTuple.remoteHost ||
			hostPortTuple.localPort != hostPortTuple.remotePort)) {
		
			remotePeerConn = new SocketLogic(null, null);
			remotePeerConn.onConnect = this._onRemotePeerConnect;
			remotePeerConn.onData = this._onPeerData;
			remotePeerConn.onConnectionClose = this._onPeerClose;
			//remotePeerConn.bind(ourHost, ourPort + portDelta * 2);
			//remotePeerConn.connect(hostPortTuple.remoteHost, hostPortTuple.remotePort + delta, {host:ourHost, port:ourPort});
			//remotePeerConn.connect(hostPortTuple.remoteHost, hostPortTuple.remotePort, {host:'0.0.0.0', port:ourPort});
			Util.log('Our guessed port is: $ourPort');
			remotePeerConn.connect(hostPortTuple.remoteHost, hostPortTuple.remotePort);
			
			Util.log('P2P: Trying remote connection to ${hostPortTuple.remoteHost}:${hostPortTuple.remotePort}.');
			
			Sys.sleep(5);
			
			Util.log('Starting listen socket at 0.0.0.0:$ourPort');
			
			var listenPeerConn = new SocketLogic(null, null);
			listenPeerConn.onData = function(data:Dynamic) {
				Util.log('Data from: ${listenPeerConn.remoteHost}:${listenPeerConn.remotePort}' );
				this._onPeerData(data);
			}
			listenPeerConn.bind('0.0.0.0', ourPort);
			listenPeerConn.listen();
			
			
			
		}
	*/
		
		
	}
	
	private function findDelta(ports:Array<Int>):Int {
		
		var foundPorts:Array<Int> = new Array<Int>();
		var deltas:Map<Int, Int> = new Map<Int, Int>();
		
		var testConn:Socket = new Socket();
		testConn.connect(new Host(this.host), this.port);
		var bindHost:String = testConn.host().host.toString();
		var bindPort:Int = testConn.host().port;
		testConn.close();
		
		var deltaSocks:Array<TCPSocketLogic> = new Array<TCPSocketLogic>();
		var respondPorts:Map<Int, Int> = new Map<Int, Int>();
		
		var responds:Int = 0;
		
		Util.log('Ports to check: $ports');
		
		for (deltaCheckPort in ports) {

			Util.log('Checking ${this.host}:$deltaCheckPort');
			
			var deltaSock:TCPSocketLogic = new TCPSocketLogic(null, null);
			deltaSocks.push(deltaSock);
			
			deltaSock.onConnect = function() {
				deltaSock.send('{"cmd":"gethostport"}\n');
			}
			
			deltaSock.onData = function(data:Dynamic) {
				
				var dataStr:String = cast(data, String);
				var dataObj:Dynamic = null;
				
				try {
					dataObj = Json.parse(dataStr);
				} catch (e:Dynamic) { }
				
				if (dataObj != null && dataObj.cmd == 'hostport') {
					Util.log('Got port ${dataObj.port} for $deltaCheckPort');
					respondPorts[deltaCheckPort] = dataObj.port;
					responds++;
				}
				
			}
			
			deltaSock.connect(this.host, deltaCheckPort, { host:bindHost, port:bindPort } );
			
			Sys.sleep(0.1);
			
		}
		
		do {
			Sys.sleep(0.1);
		} while ( responds < 5 );
		
		for (i in 1...ports.length) {
			var currPort:Int = ports[i];
			var prevPort:Int = ports[i - 1];
			var delta:Int = currPort - prevPort;
			if (!deltas.exists(delta)) deltas[delta] = 0;
			deltas[delta] = deltas[delta] + 1;
		}
		
		for (deltaSock in deltaSocks) deltaSock.closeConnection();
		
		var resultDelta:Int = 0;
		var maxDeltas:Int = 0;
		
		for (key in deltas.keys()) {
			
			Util.log('Key: $key, delta: ${deltas[key]}');
			
			if (deltas[key] > maxDeltas) {
				maxDeltas = deltas[key];
				resultDelta = key;
			}
			
		}
		
		ourPort = respondPorts[ports[ports.length - 1]] + resultDelta * 2;
		
		portDelta = resultDelta;
		
		return resultDelta;
		
	}
	
	private function _onLocalPeerConnect() {
		
		Util.log('P2P: Connected to local peer.');
		
		if (remotePeerConn != null) remotePeerConn.closeConnection();
		if (remoteUdpPeerConn != null) remoteUdpPeerConn.closeConnection();
		
		if (onLocalPeerConnect != null) onLocalPeerConnect();
		
	}
	
	private function _onRemotePeerConnect() {
		
		if (localUdpPeerConn != null) localUdpPeerConn.closeConnection();
		
		if (localPeerConn != null) {
			
			if (localPeerConn.sockState != SocketLogic.SockState.CONNECTED) {
				localPeerConn.closeConnection();
			} else {
				remotePeerConn.closeConnection();
				return;
			}
			
		}
		
		Util.log('P2P: Connected to remote peer.');
		if (onRemotePeerConnect != null) onRemotePeerConnect();
		
	}
	
	private function _onPeerData(data:Dynamic) {
		
		if (onPeerData != null) onPeerData(data);
		
	}
	
	private function _onPeerClose() {
		
		if (onPeerClose != null) onPeerClose();
		
	}
	
}
