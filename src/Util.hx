package;
import neko.Lib;

/**
 * ...
 * @authors Arthur Makhonko, Michael Adeyeye Oshin
 */

enum LogLevel {
	
	NONE;		// No logging at all
	ERROR;		// Log only critical errors
	WARNING;	// Log critical errors and warnings
	ALL;		// Log all
	
}
 
class Util {
	
	public static var logLevel:LogLevel = LogLevel.ERROR;

	public static function log(msg:String) {
		
		if (logLevel == LogLevel.ALL) {
			Lib.println(msg);
		}
		
	}
	
	public static function warn(msg:String) {
		
		if (logLevel == LogLevel.ALL || logLevel == LogLevel.WARNING) {
			Lib.println('WARN: $msg');
		}
		
	}
	
	public static function error(msg:String) {
		
		if (logLevel == LogLevel.ALL || logLevel == LogLevel.WARNING || logLevel == LogLevel.ERROR) {
			Lib.println('ERROR: $msg');
		}
		
	}
	
	public static function extend(dest:Dynamic, source:Dynamic) {
		
		if (source != null) {
		
			for (field in Reflect.fields(source)) {
				
				if (Reflect.hasField(source, field)) {
					Reflect.setProperty(dest, field, Reflect.getProperty(source, field));
				}
				
			}
			
		}
		
		return dest;
		
	}
	
}
